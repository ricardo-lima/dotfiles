# dotfiles



## IPython


To create the blank configuration files, run:

```bash
ipython profile create [profilename]
```




## Konsole

#### Scripting Konsole

```bash
qdbus6 "${KONSOLE_DBUS_SERVICE}"
qdbus6 org.kde.konsole-`pidof -s konsole`


qdbus-qt5 "${KONSOLE_DBUS_SERVICE}"
qdbus-qt5 org.kde.konsole-`pidof -s konsole`


# Display methods for controlling the current window
qdbus6 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_WINDOW}"
qdbus-qt5 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_WINDOW}"


# Get total number of sessions
qdbus6 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_WINDOW}" sessionCount
qdbus-qt5 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_WINDOW}" sessionCount


# Display methods for controlling the current session
qdbus6 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}"
qdbus-qt5 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}"


# Display current session
echo "${KONSOLE_DBUS_SESSION}"


# Display methods for session 1 (change 1 to the desired session)
qdbus6 "${KONSOLE_DBUS_SERVICE}" /Sessions/1
qdbus-qt5 "${KONSOLE_DBUS_SERVICE}" /Sessions/1


# Display methods
qdbus6 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}" | awk -F '.' '{print $NF}' | awk -F '(' '{print $1}'
qdbus-qt5 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}" | awk -F '.' '{print $NF}' | awk -F '(' '{print $1}'


qdbus6 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}" | grep "konsole\.Session" | awk -F '.' '{print $NF}' | awk -F '(' '{print $1}'
qdbus-qt5 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}" | grep "konsole\.Session" | awk -F '.' '{print $NF}' | awk -F '(' '{print $1}'


# Outputs profile
qdbus6 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}" profile
qdbus-qt5 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}" profile


# Outputs environment
qdbus6 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}" environment
qdbus-qt5 "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}" environment
```



## Notes

Open Konsole's configuration window (`CTRL + SHIFT + ,`) and enable the option **Remember window size**.

This will prevent *vim-airline* and *lightline* from breaking.




## References

- [Introduction to IPython configuration](https://ipython.readthedocs.io/en/stable/config/intro.html#introduction-to-ipython-configuration)
- [The Konsole Handbook](https://docs.kde.org/stable5/en/konsole/konsole/index.html)
- [Introduction to DBUS (documentation)](https://develop.kde.org/docs/d-bus/introduction_to_dbus/)
- [Setting up KDE (script)](https://github.com/shalva97/kde-configuration-files/blob/master/scripts/setupKDE.fish)
- [Installing Fedora (minimal install](https://github.com/Zer0CoolX/Fedora-KDE-Minimal-Install-Guide/blob/master/fedora-kde-min-packages.sh)
- [Fedora Server](https://getfedora.org/en/server/download/)
- [Fedora install (automated)](https://docs.fedoraproject.org/en-US/fedora/rawhide/install-guide/advanced/Kickstart_Installations/)
- [Fedora Kickstart (automated install)](https://docs.fedoraproject.org/en-US/fedora/rawhide/install-guide/appendixes/Kickstart_Syntax_Reference/#sect-kickstart-example-pre-script)
- [Escape Codes](https://en.wikipedia.org/wiki/ANSI_escape_code)
